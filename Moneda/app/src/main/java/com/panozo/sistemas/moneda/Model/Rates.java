package com.panozo.sistemas.moneda.Model;

/**
 * Created by nicolaspanozo on 15/11/17.
 */

public class Rates {
    private String AUD;
    private String BGN;
    private String BRL;
    private String CAD;
    private String CHF;
    private String CNY;
    private String CZK;
    private String DKK;
    private String EUR;
    private String GBP;
    private String HKD;
    private String HRK;
    private String HUF;
    private String IDR;
    private String ILS;
    private String INR;
    private String JPY;
    private String KRW;
    private String MXN;
    private String MYR;
    private String NOK;
    private String NZD;
    private String PHP;
    private String PLN;
    private String RON;
    private String RUB;
    private String SEK;
    private String SGD;
    private String THB;
    private String TRY;
    private String USD;
    private String ZAR;

    public Rates(String AUD, String BGN, String BRL, String CAD, String CHF, String CNY, String CZK, String DKK, String EUR, String GBP, String HKD, String HRK, String HUF, String IDR, String ILS, String INR, String JPY, String KRW, String MXN, String MYR, String NOK, String NZD, String PHP, String PLN, String RON, String RUB, String SEK, String SGD, String THB, String TRY, String USD, String ZAR) {
        this.AUD = AUD;
        this.BGN = BGN;
        this.BRL = BRL;
        this.CAD = CAD;
        this.CHF = CHF;
        this.CNY = CNY;
        this.CZK = CZK;
        this.DKK = DKK;
        this.EUR = EUR;
        this.GBP = GBP;
        this.HKD = HKD;
        this.HRK = HRK;
        this.HUF = HUF;
        this.IDR = IDR;
        this.ILS = ILS;
        this.INR = INR;
        this.JPY = JPY;
        this.KRW = KRW;
        this.MXN = MXN;
        this.MYR = MYR;
        this.NOK = NOK;
        this.NZD = NZD;
        this.PHP = PHP;
        this.PLN = PLN;
        this.RON = RON;
        this.RUB = RUB;
        this.SEK = SEK;
        this.SGD = SGD;
        this.THB = THB;
        this.TRY = TRY;
        this.USD = USD;
        this.ZAR = ZAR;
    }

    public String getAUD() {
        return AUD;
    }

    public void setAUD(String AUD) {
        this.AUD = AUD;
    }

    public String getBGN() {
        return BGN;
    }

    public void setBGN(String BGN) {
        this.BGN = BGN;
    }

    public String getBRL() {
        return BRL;
    }

    public void setBRL(String BRL) {
        this.BRL = BRL;
    }

    public String getCAD() {
        return CAD;
    }

    public void setCAD(String CAD) {
        this.CAD = CAD;
    }

    public String getCHF() {
        return CHF;
    }

    public void setCHF(String CHF) {
        this.CHF = CHF;
    }

    public String getCNY() {
        return CNY;
    }

    public void setCNY(String CNY) {
        this.CNY = CNY;
    }

    public String getCZK() {
        return CZK;
    }

    public void setCZK(String CZK) {
        this.CZK = CZK;
    }

    public String getDKK() {
        return DKK;
    }

    public void setDKK(String DKK) {
        this.DKK = DKK;
    }

    public String getGBP() {
        return GBP;
    }

    public void setGBP(String GBP) {
        this.GBP = GBP;
    }

    public String getHKD() {
        return HKD;
    }

    public void setHKD(String HKD) {
        this.HKD = HKD;
    }

    public String getHRK() {
        return HRK;
    }

    public void setHRK(String HRK) {
        this.HRK = HRK;
    }

    public String getHUF() {
        return HUF;
    }

    public void setHUF(String HUF) {
        this.HUF = HUF;
    }

    public String getIDR() {
        return IDR;
    }

    public void setIDR(String IDR) {
        this.IDR = IDR;
    }

    public String getILS() {
        return ILS;
    }

    public void setILS(String ILS) {
        this.ILS = ILS;
    }

    public String getINR() {
        return INR;
    }

    public void setINR(String INR) {
        this.INR = INR;
    }

    public String getJPY() {
        return JPY;
    }

    public void setJPY(String JPY) {
        this.JPY = JPY;
    }

    public String getKRW() {
        return KRW;
    }

    public void setKRW(String KRW) {
        this.KRW = KRW;
    }

    public String getMXN() {
        return MXN;
    }

    public void setMXN(String MXN) {
        this.MXN = MXN;
    }

    public String getMYR() {
        return MYR;
    }

    public void setMYR(String MYR) {
        this.MYR = MYR;
    }

    public String getNOK() {
        return NOK;
    }

    public void setNOK(String NOK) {
        this.NOK = NOK;
    }

    public String getNZD() {
        return NZD;
    }

    public void setNZD(String NZD) {
        this.NZD = NZD;
    }

    public String getPHP() {
        return PHP;
    }

    public void setPHP(String PHP) {
        this.PHP = PHP;
    }

    public String getPLN() {
        return PLN;
    }

    public void setPLN(String PLN) {
        this.PLN = PLN;
    }

    public String getRON() {
        return RON;
    }

    public void setRON(String RON) {
        this.RON = RON;
    }

    public String getRUB() {
        return RUB;
    }

    public void setRUB(String RUB) {
        this.RUB = RUB;
    }

    public String getSEK() {
        return SEK;
    }

    public void setSEK(String SEK) {
        this.SEK = SEK;
    }

    public String getSGD() {
        return SGD;
    }

    public void setSGD(String SGD) {
        this.SGD = SGD;
    }

    public String getTHB() {
        return THB;
    }

    public void setTHB(String THB) {
        this.THB = THB;
    }

    public String getTRY() {
        return TRY;
    }

    public void setTRY(String TRY) {
        this.TRY = TRY;
    }

    public String getUSD() {
        return USD;
    }

    public void setUSD(String USD) {
        this.USD = USD;
    }

    public String getZAR() {
        return ZAR;
    }

    public void setZAR(String ZAR) {
        this.ZAR = ZAR;
    }

    public String getEUR() {
        return EUR;
    }

    public void setEUR(String EUR) {
        this.EUR = EUR;
    }

    @Override
    public String toString() {
        return "Rates{" +
                "AUD='" + AUD + '\'' +
                ", BGN='" + BGN + '\'' +
                ", BRL='" + BRL + '\'' +
                ", CAD='" + CAD + '\'' +
                ", CHF='" + CHF + '\'' +
                ", CNY='" + CNY + '\'' +
                ", CZK='" + CZK + '\'' +
                ", DKK='" + DKK + '\'' +
                ", EUR='" + EUR + '\'' +
                ", GBP='" + GBP + '\'' +
                ", HKD='" + HKD + '\'' +
                ", HRK='" + HRK + '\'' +
                ", HUF='" + HUF + '\'' +
                ", IDR='" + IDR + '\'' +
                ", ILS='" + ILS + '\'' +
                ", INR='" + INR + '\'' +
                ", JPY='" + JPY + '\'' +
                ", KRW='" + KRW + '\'' +
                ", MXN='" + MXN + '\'' +
                ", MYR='" + MYR + '\'' +
                ", NOK='" + NOK + '\'' +
                ", NZD='" + NZD + '\'' +
                ", PHP='" + PHP + '\'' +
                ", PLN='" + PLN + '\'' +
                ", RON='" + RON + '\'' +
                ", RUB='" + RUB + '\'' +
                ", SEK='" + SEK + '\'' +
                ", SGD='" + SGD + '\'' +
                ", THB='" + THB + '\'' +
                ", TRY='" + TRY + '\'' +
                ", USD='" + USD + '\'' +
                ", ZAR='" + ZAR + '\'' +
                '}';
    }
}
