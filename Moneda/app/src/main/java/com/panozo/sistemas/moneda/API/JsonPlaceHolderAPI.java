package com.panozo.sistemas.moneda.API;

/**
 * Created by nicolaspanozo on 12/11/17.
 */

import com.panozo.sistemas.moneda.Model.Latest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderAPI {
    @GET("latest")
    Call<Latest> getLatest(@Query("base") String base, @Query("symbols") String simbols);
}
