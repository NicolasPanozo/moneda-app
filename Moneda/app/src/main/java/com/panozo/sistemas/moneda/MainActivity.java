package com.panozo.sistemas.moneda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.panozo.sistemas.moneda.API.JsonPlaceHolderAPI;
import com.panozo.sistemas.moneda.Dialogo.DialogoSeleccion;
import com.panozo.sistemas.moneda.Model.Latest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Callback<Latest> {

    private static TextView lbl_moneda_desde;
    private static TextView lbl_moneda_hasta;
    private LinearLayout linear_moneda_desde;
    private LinearLayout linear_moneda_hasta;
    private EditText txt_moneda_desde;
    private EditText txt_moneda_hasta;
    private Button btn_reverse;
    private Button btn_convert;

    private Latest latest;
    private static String base = "EUR";
    private static String symbols = "USD";

    public static String BASE_URL = "https://api.fixer.io/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lbl_moneda_desde = (TextView) findViewById(R.id.lbl_moneda_desde);
        lbl_moneda_hasta = (TextView) findViewById(R.id.lbl_moneda_hasta);
        linear_moneda_desde = (LinearLayout) findViewById(R.id.linear_moneda_desde);
        linear_moneda_desde.setOnClickListener(this);
        linear_moneda_hasta = (LinearLayout) findViewById(R.id.linear_moneda_hasta);
        linear_moneda_hasta.setOnClickListener(this);
        txt_moneda_desde = (EditText) findViewById(R.id.txt_moneda_desde);
        txt_moneda_hasta = (EditText) findViewById(R.id.txt_moneda_hasta);
        btn_reverse = (Button) findViewById(R.id.btn_reverse);
        btn_reverse.setOnClickListener(this);
        btn_convert = (Button) findViewById(R.id.btn_convert);
        btn_convert.setOnClickListener(this);

        loadLatest(base, symbols);

        lbl_moneda_desde.setText("EUR");
        lbl_moneda_hasta.setText("USD");

    }

    private void loadLatest(String base, String symbols) {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        JsonPlaceHolderAPI jsonPlaceHolderAPI = retrofit.create(JsonPlaceHolderAPI.class);
        Call<Latest> call = jsonPlaceHolderAPI.getLatest(base, symbols);
        call.enqueue(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.linear_moneda_desde:
                DialogoSeleccion dialogDesde = new DialogoSeleccion();
                Bundle bundleDesde = new Bundle();
                bundleDesde.putInt("selector", 1);
                dialogDesde.setArguments(bundleDesde);
                dialogDesde.show(getSupportFragmentManager(), "tagMoneda");
                break;
            case R.id.linear_moneda_hasta:
                DialogoSeleccion dialogHasta = new DialogoSeleccion();
                Bundle bundleHasta = new Bundle();
                bundleHasta.putInt("selector", 2);
                dialogHasta.setArguments(bundleHasta);
                dialogHasta.show(getSupportFragmentManager(), "tagMoneda");
                break;
            case R.id.btn_convert:
                loadLatest(base, symbols);
                break;
            case R.id.btn_reverse:
                reverse();
                break;

        }
    }

    private void reverse(){
        String lbl_aux = lbl_moneda_desde.getText().toString();
        lbl_moneda_desde.setText(lbl_moneda_hasta.getText());
        base = lbl_moneda_hasta.getText().toString();
        symbols = lbl_aux;
        lbl_moneda_hasta.setText(lbl_aux);
        loadLatest(base, symbols);

    }

    public static void cambiarEtiqueta(String t, int selector){
        if(selector == 1){
            lbl_moneda_desde.setText(t);
            base = t;
        }else{
            lbl_moneda_hasta.setText(t);
            symbols = t;
        }
    }

    private String getValue(){
        if(symbols.compareTo("AUD")==0){
            return latest.getRates().getAUD();
        }
        if(symbols.compareTo("BGN")==0){
            return latest.getRates().getBGN();
        }
        if(symbols.compareTo("BRL")==0){
            return latest.getRates().getBRL();
        }
        if(symbols.compareTo("CAD")==0){
            return latest.getRates().getCAD();
        }
        if(symbols.compareTo("CHF")==0){
            return latest.getRates().getCHF();
        }
        if(symbols.compareTo("CNY")==0){
            return latest.getRates().getCNY();
        }
        if(symbols.compareTo("CZK")==0){
            return latest.getRates().getCZK();
        }
        if(symbols.compareTo("DKK")==0){
            return latest.getRates().getDKK();
        }
        if(symbols.compareTo("EUR")==0){
            return latest.getRates().getEUR();
        }
        if(symbols.compareTo("GBP")==0){
            return latest.getRates().getGBP();
        }
        if(symbols.compareTo("HKD")==0){
            return latest.getRates().getHKD();
        }
        if(symbols.compareTo("HRK")==0){
            return latest.getRates().getHRK();
        }
        if(symbols.compareTo("HUF")==0){
            return latest.getRates().getHUF();
        }
        if(symbols.compareTo("IDR")==0){
            return latest.getRates().getIDR();
        }
        if(symbols.compareTo("ILS")==0){
            return latest.getRates().getILS();
        }
        if(symbols.compareTo("INR")==0){
            return latest.getRates().getINR();
        }
        if(symbols.compareTo("JPY")==0){
            return latest.getRates().getJPY();
        }
        if(symbols.compareTo("KRW")==0){
            return latest.getRates().getKRW();
        }
        if(symbols.compareTo("MXN")==0){
            return latest.getRates().getMXN();
        }
        if(symbols.compareTo("MYR")==0){
            return latest.getRates().getMYR();
        }
        if(symbols.compareTo("NOK")==0){
            return latest.getRates().getNOK();
        }
        if(symbols.compareTo("NZD")==0){
            return latest.getRates().getNZD();
        }
        if(symbols.compareTo("PHP")==0){
            return latest.getRates().getPHP();
        }
        if(symbols.compareTo("PLN")==0){
            return latest.getRates().getPLN();
        }
        if(symbols.compareTo("RON")==0){
            return latest.getRates().getRON();
        }
        if(symbols.compareTo("RUB")==0){
            return latest.getRates().getRUB();
        }
        if(symbols.compareTo("SEK")==0){
            return latest.getRates().getSEK();
        }
        if(symbols.compareTo("SGD")==0){
            return latest.getRates().getSGD();
        }
        if(symbols.compareTo("THB")==0){
            return latest.getRates().getTHB();
        }
        if(symbols.compareTo("TRY")==0){
            return latest.getRates().getTRY();
        }
        if(symbols.compareTo("USD")==0){
            return latest.getRates().getUSD();
        }
        if(symbols.compareTo("ZAR")==0){
            return latest.getRates().getZAR();
        }
        return "";
    }

    private double calcularConversion(double moneda_desde, double symbols_value){
        return moneda_desde*symbols_value;

    }


    @Override
    public void onResponse(Call<Latest> call, Response<Latest> response) {
        if(response.isSuccessful()){
            latest = response.body(); // Guardo una instancia del response.

            try {
                double symbols_value = Double.valueOf(getValue());
                double moneda_desde = Double.valueOf(txt_moneda_desde.getText().toString());
                double resultado = calcularConversion(moneda_desde, symbols_value);
                txt_moneda_hasta.setText(String.valueOf(resultado));
            }catch (Exception e){
                Toast.makeText(this, "Conversión no valida", Toast.LENGTH_SHORT).show();
            }

        }else{
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Latest> call, Throwable t) {
        t.printStackTrace();
    }
}
